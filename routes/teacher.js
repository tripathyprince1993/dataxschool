const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});

const Students=mongoose.model("Teachers");

router.get("/",async(req,res)=>{
  
       const teacher=await Teachers.find({})
       res.send(teacher);
   
});
router.post("/",async(req,res)=>{
  
        const teachers=new Teachers();
       teachers.class_teacher_id=req.body.class_teacher_id;
       teachers.classrooms=req.body.classrooms;
       teachers.first_name=req.body.first_name;
       teachers.middle_name=req.body.middle_name;
       teachers.last_name=req.body.last_name;
       teachers.birth_date=req.body.birth_date;
       teachers.sex=req.body.sex;
       teachers.religion=req.body.religion;
       teachers.blood_group=req.body.blood_group;
       teachers.address=req.body.address;
       teachers.phone=req.body.phone;
       teachers.email=req.body.email;
       teachers.password=req.body.password;
       teachers.updated_by=req.body.updated_by;
       teachers.updation_date=req.body.updation_date;
       teachers.created_by=req.body.created_by;
       teachers.creation_date=req.body.creation_date;
        await teachers.save();
        res.send(teachers);
})
module.exports=router;