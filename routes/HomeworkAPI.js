const route=require('express').Router();
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
require('dotenv').config();
var session = require('express-session');
mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Students=mongoose.model("Students");
const ClassRoom=mongoose.model('Classrooms');
const Homework=mongoose.model("Homeworks");
const Login=mongoose.model('Login');


route.get("/v1/homework", async(req,res)=>{

    var studentId=req.body.studentID;
    var token=req.body.token_id;

    var authorizationKey = req.headers.authorization;
    var serverAuthKey = process.env.AuthrizationKey;
    if(authorizationKey==serverAuthKey)
    {
        var toakenQuery={tokenId:token}
        var tokenDetail= await Login.findOne(toakenQuery,{tokenId:1,_id:0})
        if(tokenDetail!=null)
        {
            var query={_id:studentId}
            var studentsDetails= await Students.findOne(query,{first_name:1,middle_name:1,last_nmae:1,roll_No:1,class_id:1});
            var studentData=studentsDetails.toObject();
            var firstName=studentData.first_name;
            var middleName=studentData.middle_name;
            var lastName=studentData.last_name;

            var nameText=firstName+" "+middleName+" "+lastName;

            var classId=studentData.class_id;

            var cQuery={_id:classId}
            var classDetails= await ClassRoom.findOne(cQuery,{class_Name:1,class_section:1})
            var classData=classDetails.toObject();
            var className=classData.class_Name;
            var classSection=classData.class_section;

            var homeworkQuery={SectionID:classId}
            var homeworkDetails= await Homework.find(homeworkQuery,{homework_date:1,comments:1,_id:0})
            console.log(homeworkDetails);
           
             res.setHeader('Content-Type', 'application/json');
             res.send(JSON.stringify({Name:nameText,Class:className,Section:classSection,homework:homeworkDetails},null,3));
            //res.status(200).send("True");
            

        }
        else
        {
            res.status(200).send("False");
        }
    }
    else
    {
        res.status(200).send("False");
    }

});
module.exports=route;
