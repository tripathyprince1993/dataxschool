const route=require('express').Router();
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
require('dotenv').config();
var session = require('express-session');
mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });


const Students=mongoose.model("Students");
const ClassRoom=mongoose.model('Classrooms');
const Login=mongoose.model('Login');

route.get("/v1/profileView", async(req, res)=>{
    var appid = req.headers.app_id;
    var studentId=req.body.studentID;
    var token=req.body.token_id;

    var authorizationKey = req.headers.authorization;
    var serverAuthKey = process.env.AuthrizationKey;
    var serverApp_Id =  process.env.UniqApp_id;

    if(authorizationKey==serverAuthKey)
    {
        var toakenQuery={tokenId:token}
        var tokenDetail= await Login.findOne(toakenQuery,{tokenId:1,_id:0})
        //var tokenData=tokenDetail.toObject();
        if(tokenDetail!=null)
        {
            var query={_id:studentId}
            var studentsDetails= await Students.findOne(query,{admission_number:1,first_name:1,middle_name:1,last_nmae:1,roll_No:1,birth_date:1,sex:1,father_name:1,mother_name:1,father_Contact_No:1,mother_contact_No:1,email:1,class_id:1});
            var studentData=studentsDetails.toObject();
            var firstName=studentData.first_name;
            var middleName=studentData.middle_name;
            var lastName=studentData.last_name;
            var roll_no=studentData.roll_No;
            var admissionNumber=studentData.admission_number;
            var birthDate=studentData.birth_date;
            var sex=studentData.sex;
            var fatherNumber=studentData.father_Contact_No;
            var motherNumber=studentData.mother_contact_No;
            var fatherName=studentData.father_name;
            var motherName=studentData.mother_name;
            var mail=studentData.email

            var nameText=firstName+" "+middleName+" "+lastName;

            var classId=studentData.class_id;

            var cQuery={_id:classId}
            var classDetails= await ClassRoom.findOne(cQuery,{class_Name:1,class_section:1})
            var classData=classDetails.toObject();
            var className=classData.class_Name;
            var classSection=classData.class_section;
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({Name:nameText,Class:className,Section:classSection,Roll_No:roll_no,Adm_No:admissionNumber,DOB:birthDate,Sex:sex,Father_Name:fatherName,Mother_Name:motherName,Father_Number:fatherNumber,Mother_Number:motherNumber,Email:mail},null,3));

        }
        else
        {
            res.status(200).send("False");
        }
    }
    else
    {
        res.status(200).send("False");
    }

});
module.exports = route;
