const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });
const Attendance = mongoose.model("Attendance");
const Classrooms = mongoose.model("Classrooms");
const Students=mongoose.model("Students");
const Calenders=mongoose.model("Calenders");
router.get("/", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/attendence.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});



router.get("/View", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/view_attendence.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});



router.get("/Update", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/update_attendence.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});






var status = false
// APIs will begin after here
router.post("/API/Attendence_Submit", async (req, res) => {
    
        console.log(req.body);
        var body = JSON.stringify(req.body);
      //  console.log(body.length)
        var attendanceData = JSON.parse(body);
        var jsonData = JSON.stringify(attendanceData)
     //  console.log(jsonData);
        var count = 0 ;
     req.body.item.forEach( async function(values) {
        // `item` is the next item in the array
        // `index` is the numeric position in the array, e.g. `array[index] == item`
          //console.log(JSON.stringify(item));
          //var xx = JSON.stringify(item1,value)
          var eachdata = values;
          //  console.log(count);
          count+=1;
      //    console.log(eachdata["students"]);
      const class_attendance = new Attendance();
        
         class_attendance.students = eachdata["students"];
        class_attendance.attendance_date =eachdata["attendance_date"];
        class_attendance.status = eachdata["status"];
        class_attendance.reason = eachdata["reason"];
        class_attendance.remarks = eachdata["remarks"];
        class_attendance.Class_id = eachdata["Class_id"];
        class_attendance.created_by = eachdata["created_by"];
        class_attendance.creation_date = eachdata["creation_date"];
        class_attendance.updated_by = eachdata["updated_by"];
        class_attendance.updation_date = eachdata["updation_date"];
       // var xx = await class_attendance.save();
         //console.log(xx);
         await  class_attendance.save((err) => {
                // console.log(docs);
                 if (err) {
                        console.log(err.message,"-----fff  ----");
                        res.send(err);
                 }

          });

        });
      
        var formData = {
                Result: "Successfully saved attendence"
             }
     
   res.send(JSON.stringify(formData));

});


// APIs will begin after here
router.post("/API/ViewAttendence", async (req, res) => {

        console.log(req.body.attendence_date,req.body.Class_id)
        var classid = mongoose.Types.ObjectId(req.body.Class_id);
        var query = { attendance_date : req.body.attendence_date , Class_id : classid };
        const attendanceData =await Attendance.find(query).populate({ path: 'students', select: ['first_name','middle_name','last_name','roll_No']});

           console.log(attendanceData)

     
   res.send(attendanceData);

});


router.post("/API/CheckAttendence", async (req, res) => {

        console.log(req.body.attendence_date,req.body.Class_id)
      
        var query = { attendance_date : req.body.attendence_date , Class_id : req.body.Class_id };
        const attendancecount =await Attendance.find(query).count();

        var data_return = {
                attendancecount: attendancecount
         }
         res.send(data_return);
           //console.log(attendancecount)

});




router.post("/API/GetbyStudent", async (req, res) => {

       // console.log(req.body.attendence_date,req.body.Class_id)
      
       // var query = { attendance_date : req.body.attendence_date , Class_id : req.body.Class_id };
//        const attendancecount =await Attendance.find(query).count();
      //  const attendanceData =await Calenders.find().populate({ path: 'attendances'});
        //console.log(attendanceData)

        await Calenders.aggregate([
             { $lookup:
                  {
                    from: 'attendances',
                    localField: 'calender_date',
                    foreignField: 'attendance_date',
                    as: 'attendence'
                  }
                }
              ])



        var data_return = {
                attendancecount: attendanceData
         }
         res.send(data_return);
           //console.log(attendancecount)


});

module.exports = router;