const route=require('express').Router();
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
require('dotenv').config();
var session = require('express-session');
var crypto=require("crypto");
var generator=require('generate-password');
mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Parent = mongoose.model("Parents");
const Students=mongoose.model("Students");
const ClassRoom=mongoose.model('Classrooms');
const Login=mongoose.model("Login");



route.post("/v1/login",async(req,res)=>{

    var userName=req.body.mobileNumber;
    var userPassWord=req.body.password;
    var appid = req.headers.app_id;

    const key = crypto.randomBytes(32);
    const iv = crypto.randomBytes(16);

    function encrypt(text) {
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
       }

    // function decrypt(text) {
    //     let iv = Buffer.from(text.iv, 'hex');
    //     let encryptedText = Buffer.from(text.encryptedData, 'hex');
    //     let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    //     let decrypted = decipher.update(encryptedText);
    //     decrypted = Buffer.concat([decrypted, decipher.final()]);
    //     return decrypted.toString();
    //    }

    //    var data=encrypt(appid)

       var appId=decrypt(appid);
    

    var authorizationKey = req.headers.authorization;
    var serverAuthKey = process.env.AuthrizationKey;
    var serverApp_Id =  process.env.UniqApp_id;

    if(authorizationKey==serverAuthKey && serverApp_Id==appId)
    {
        var userfatherQuery={father_number:userName}
       // var userfatherpasswordQuery={father_password:userPassWord}
        var fatherDetails=await Parent.findOne(userfatherQuery,{father_password:1});
        console.log(fatherDetails);

        var usermotherQuery={mother_number:userName}
       // var usermotherpasswordQuery={mother_password:userPassWord}
        var motherDetails=await Parent.findOne(usermotherQuery,{mother_password:1});
        console.log(motherDetails);


        if(fatherDetails!=null && fatherDetails.father_password==userPassWord)
        {
            var tokenId=Math.random().toString().replace('0.','')
            const login=new Login()
            login.tokenId=tokenId;
            login.parentId=fatherDetails._id
            await login.save()
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({token_id:tokenId},null,3));
        }

        else if(motherDetails !=null && motherDetails.mother_password==userPassWord )
        {
            var tokenId=Math.random().toString().replace('0.','')
            console.log(tokenId)
            const login=new Login()
            login.tokenId=tokenId;
            login.parentId=motherDetails._id
            await login.save()
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({token_id:tokenId},null,3));

        }
        else
        {
            res.status(200).send("False");
        }

    }
});

route.get("/v1/getStudentDetails",async(req,res)=>{
    var tokenCode=req.body.tokenId;
    var loginQuery={tokenId:tokenCode}
    var loginDetails=await Login.findOne(loginQuery,{});
    var ParentId=loginDetails.parentId;
    var studentQuery={parent_id:ParentId}
    var studentsDetails= await Students.findOne(studentQuery,{first_name:1,middle_name:1,last_name:1,class_id:1,_id:1})
    var studentData=studentsDetails.toObject();
    var firstName=studentData.first_name;
    var middleName=studentData.middle_name;
    var lastName=studentData.last_name;
    var Id=studentData._id;
    var nameText=firstName+" "+middleName+" "+lastName;

    var classId=studentData.class_id;

    var class_query={_id:classId}
    var ClassData=await ClassRoom.findOne(class_query,{});
    var classData=ClassData.toObject();
    var className=classData.class_Name;
    var classSection=classData.class_section;
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({studentId:Id,Name:nameText,Class:className,class:className,Section:classSection},null,3));
});

route.post("/v1/logout",async(req,res)=>{
    var tokenCode=req.body.tokenId
    await Login.deleteOne({tokenId:tokenCode});
    res.status(200).send("False");
})

module.exports = route;
