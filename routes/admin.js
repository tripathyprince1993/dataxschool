const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();
var session = require('express-session');

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Admins = mongoose.model("Admins");

router.get("/", async (req, res) => {

       sess = req.session;
       //Session set when user Request our app via URL
     //  console.log(sess);
       if (sess.MobNumber) {
              /*
              * This line check Session existence.
              * If it existed will do some action.
              */
              res.render(__basedir + '/views/index.ejs', {
                     Name: sess.Name,
                     mobno: sess.MobNumber,
                     userid:sess.id
              });
       }
       else {
              res.redirect("/Login");
       }

       //const admin=await Admins.find({})
       //res.send(admin);

});
router.post("/", async (req, res) => {

       const admins = new Admins();
       
       admins.admin_id = req.body.admin_id;
       admins.first_name = req.body.first_name;
       admins.middle_name = req.body.middle_name;
       admins.last_name = req.body.last_name;
       admins.email = req.body.email;
       admins.password = req.body.password;
       admins.contact_number = req.body.contact_number;
       admins.created_by = req.body.created_by;
       admins.creation_date = req.body.creation_date;
       admins.updated_by = req.body.updated_by;
       admins.updation_date = req.body.updation_date;

       await admins.save();
       res.send(admins);
})
module.exports = router;