const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});
const CalenderYear=mongoose.model('CalenderYear');

router.get("/",async(req,res)=>{
    
    const calenderyear=await CalenderYear.find({});
    res.send(calenderyear);

});

router.post("/",async(req,res)=>{
  
    const calenderyear =new CalenderYear();
    calenderyear.calender_year=req.body.calender_year
    await calenderyear.save();
        res.send(calenderyear);
});
module.exports=router;