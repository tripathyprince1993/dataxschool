const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });


const Classrooms = mongoose.model("Classrooms");
const NotieBoards = mongoose.model("Noticeboards");

router.get("/", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/Noticeboard.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});

router.get("/Edit", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/Edit_Noticeboard.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});

router.get("/View", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/View_Noticeboard.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});



// APIs will begin after here
router.post("/API/CreateNotice", async (req, res) => {
    
        console.log(req.body);
       // var body = JSON.stringify(req.body);        
       var query = [] ;
       var TodayDay =   Date();

     
        query.push({  notice_body:req.body.Notice_body, notice_image:req.body.notice_image, notice_Postdate :req.body.NoticeDate,  status:req.body.status,   creation_date:TodayDay /* created_by:req.body.created_by, updated_by:req.body.created_by, updation_date:TodayDay */});
                console.log(query)
          
                NotieBoards.insertMany(query, function(error, docs) {
                        if (error){ 
                        return console.error(error);
                        } else {
                        console.log("Notices  inserted to noticeboard");
                        var formData = {
                                Result: "Successfully saved Notie"
                             }
                     
                   res.send(JSON.stringify(formData));
                }
});
});



module.exports = router;