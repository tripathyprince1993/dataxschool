//var router=require('express').Router();
const route=require('express').Router();
const mongoose=require('mongoose');
const bodyparser=require('body-parser');
require('dotenv').config();
var session = require('express-session');
mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });



const Students=mongoose.model('Students');
const Attendance=mongoose.model('Attendance');
const Calender=mongoose.model('Calenders');
const ClassRoom=mongoose.model('Classrooms');
const Calenderyear=mongoose.model('CalenderYear');

route.get("/v1/getStudentAttendance", async (req,res) =>{
    var student_id=req.body.studentId;
    var token_id=req.body.tokenId;

    var authorizationKey = req.headers.authorization;
    var serverAuthKey = process.env.AuthrizationKey;

    var tokenCode="123raviranjan986#";
    var flag_open="Open";
    var flag_close="Closed";

    if(authorizationKey == serverAuthKey && token_id==tokenCode)
    {
        // Data from students Collection
        var student_query={_id:student_id}
        const student_data=await Students.findOne(student_query,{});
        var studentData=student_data.toObject();
        var firstName=studentData.first_name;
        var middleName=studentData.middle_name;
        var lastName=studentData.last_name;
        var classId=studentData.class_id;
        var nameText=firstName+" "+middleName+" "+lastName;


         // Data from ClassRoom Collection
        var class_query={_id:classId}
        var ClassData=await ClassRoom.findOne(class_query,{});
        var classData=ClassData.toObject();
        var className=classData.class_Name;
        var classSection=classData.class_section;


        // Data from Attendance Collection
         var atten_query={students:student_id};
         var attendanceData=await Attendance.find(atten_query,{ attendance_date:1,students:1, status:1, reason:1,_id:0})

         //Data from calenderyear table
          var session="2019-2020";
          //var calenderyear=await Calenderyear.find({},{calender_year:1,_id:1})
           var calenderyear=await Calenderyear.find({})
          // console.log(calenderyear);

          
      //Data from Calender table
        var cal_query={calender_session:session};
        var calenderDate= await Calender.find(cal_query,{select_type:1,calender_session:1,calender_date:1,_id:0})

        // for(var i=0;i<calenderDate.length; i++)
        // {
          //Attendance table data
          for(var i=0; i<attendanceData.length; i++){
          var x= attendanceData[i].attendance_date.getFullYear()+'-' + (attendanceData[i].attendance_date.getMonth()+1) + '-'+attendanceData[i].attendance_date.getDate();
           var splitdate = x.toString().split("-");
           year = splitdate[0],
           month = splitdate[1],
           day = splitdate[2]
           //console.log(year+" "+month+" "+day );
          }
          //Calender Table Data
          for(var i=0; i<calenderDate.length; i++){
            var y= calenderDate[i].calender_date.getFullYear()+'-' + (calenderDate[i].calender_date.getMonth()+1) + '-'+calenderDate[i].calender_date.getDate();
             var splitdate = y.toString().split("-");
             calender_year = splitdate[0],
             calender_month = splitdate[1],
             calender_day = splitdate[2]
             //console.log(calender_year+" "+calender_month+" "+calender_day );
          }
          for(var i=0; i<calenderDate.length;i++)
          {
            var c=(calenderDate[i].select_type==flag_open);
           // console.log(c);
          }
          if(c)
          {
            if(year[i]==calender_year[i])
            {
              for(var j=0; j<month; j++)
              {
                switch(j){
                  case 0:
                  {

                          //Code for filtring data month wise
                          var attendanceData1= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:1}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData1 = attendanceData1.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData1);
                          // if(attendanceData1.length==0)
                          // {
                          //   console.log("hii");
                   
                    break;
                  }
                
                  case 1:
                  {
                          //Code for filtring data month wise
                          var attendanceData2= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:2}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData2 = attendanceData2.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData2);
                  }
                  case 2:
                  {
                          //Code for filtring data month wise
                          var attendanceData3= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:3}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData3 = attendanceData3.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData3);
                  }
                  case 3:
                  {
                          //Code for filtring data month wise
                          var attendanceData4= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:4}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData4 = attendanceData4.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData4);
                          break;
                  } 
                  case 4:
                  {
                   
                          //Code for filtring data month wise
                          var attendanceData5= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:5}}
                         ]);
                         //code for filtring data student_id wise
                          attendanceData5 = attendanceData5.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData5);
                   
                      break;
                  }
                  case 5:
                  {
                        //Code for filtering data month wise
                          var attendanceData6= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:6}}
                         ]);
                         //code for filtering data student_id wise
                          attendanceData6 = attendanceData6.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData6);      
                      break;
                  }
                  case 6:
                  {
                          //Code for filtring data month wise
                          var attendanceData7= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:7}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData7 = attendanceData7.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData7);
                    break;
                  }
                  case 7:
                  {
                          //Code for filtring data month wise
                          var attendanceData8= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:8}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData8 = attendanceData8.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData8);
                          
                    break;
                  }
                  case 8:
                  {
                          //Code for filtring data month wise
                          var attendanceData9= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:9}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData9 = attendanceData9.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData9);
                            
                    break;
                  }
                  case 9:
                  {
                   
                          //Code for filtring data month wise
                          var attendanceData10= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:10}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData10 = attendanceData10.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData10);
                            
                    break;
                  }
                  case 10:
                  {
                          //Code for filtring data month wise
                          var attendanceData11= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:11}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData11 = attendanceData11.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData11);
                                
                    break;
                  }
                  case 11:
                  {
                    
                          //Code for filtring data month wise
                          var attendanceData12= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:12}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData12 = attendanceData12.filter(function(item){
                          return (item.students == student_id);
                          });
                          console.log(attendanceData12);
                                
                    break;
                  }
                  default :
                  {

                  }
                
                }
              } 
            }
            else
            {
              console.log("ravi");
            }

            //JSON Data  
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({Name:nameText,Class:className,Section:classSection,Jan:attendanceData1,Feb:attendanceData2,March:attendanceData3,April:attendanceData4,May:attendanceData5,June:attendanceData6,July:attendanceData7,Aug:attendanceData8,Sep:attendanceData9,Oct:attendanceData10,Nov:attendanceData11,Dec:attendanceData12},null,3)); 
          }
          //condition for school is close
          else
          {
             var flag_close='{"status":"Closed"}';
             var School_status=JSON.parse(flag_close);
             

              var close_query={select_type:"Closed"}
              var closeStatus= await Calender.find(close_query,{select_type:1,calender_session:1,calender_date:1,_id:0})
              console.log(closeStatus);
              for(var k=0; k<closeStatus.length; k++)
              {
                var date= closeStatus[k].calender_date.getFullYear()+'-' + (closeStatus[k].calender_date.getMonth()+1) + '-'+closeStatus[k].calender_date.getDate();
                console.log(date);
                var splitdate = date.toString().split("-");
                closeMonth = splitdate[1]
                professional = Object.assign({}, School_status)
                Object.assign(professional, {
                  "Date":date
                })
              }   

            if(year[i]==calender_year[i])
            {
              for(var j=0; j<month; j++)
              {
                switch(j){
                  case 0:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {
                          var attendanceData1= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:1}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData1 = attendanceData1.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData1.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData1);           
                        }
                        else
                        {
                           var attendanceData1= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:1}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData1 = attendanceData1.filter(function(item){
                           return (item.students == student_id);
                           });
                         }  
                   
                    break;
                  }
                
                  case 1:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData2= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:2}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData2 = attendanceData2.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData2.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData2);
                        }
                        else
                        {
                           var attendanceData2= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:2}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData2 = attendanceData2.filter(function(item){
                           return (item.students == student_id);
                           });
                         }
                        break;
                  }
                  case 2:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData3= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:3}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData3 = attendanceData3.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData3.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          
                          console.log(attendanceData3);
                        }
                        else
                        {
                           var attendanceData3= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:3}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData3 = attendanceData3.filter(function(item){
                           return (item.students == student_id);
                           });
                         }
                        break;
                  }
                  case 3:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData4= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:4}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData4 = attendanceData4.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData4.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData4);
                        }
                        else
                        {
                           var attendanceData4= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:4}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData4 = attendanceData4.filter(function(item){
                           return (item.students == student_id);
                           });
                         }
                        break;  
                  } 
                  case 4:
                  {
                   
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData5= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:5}}
                         ]);
                         //code for filtring data student_id wise
                          attendanceData5 = attendanceData5.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData5.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                         // console.log(attendanceData5);
                       }
                       else
                       {
                          var attendanceData5= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:5}}
                         ]);
                        //code for filtring data student_id wise
                          attendanceData5 = attendanceData5.filter(function(item){
                          return (item.students == student_id);
                          });
                        }
                   
                      break;
                  }
                  case 5:
                  {

                        //Code for filtering data month wise
                        if(j==closeMonth-1)
                        {
                          console.log("hii");
                          var attendanceData6= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:6}}
                         ]);
                         //code for filtering data student_id wise
                          attendanceData6 = attendanceData6.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData6.push(
                          Object.assign(professional, {
                            "students":student_id,
                            "Date":date,
                            "month":month
                          }))
                         // console.log(attendanceData6);
                       } 
                       else
                       {
                          var attendanceData6= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:6}}
                         ]);
                        //code for filtring data student_id wise
                          attendanceData6 = attendanceData6.filter(function(item){
                          return (item.students == student_id);
                          });
                        }       
                      break;
                  }
                  case 6:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData7= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:7}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData7 = attendanceData7.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData7.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData7);
                        }
                        else
                        {
                           var attendanceData7= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:7}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData7 = attendanceData7.filter(function(item){
                           return (item.students == student_id);
                           });
                         }  
                    break;
                  }
                  case 7:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData8= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:8}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData8 = attendanceData8.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData8.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData8);
                        }
                        else
                        {
                           var attendanceData8= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:8}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData8 = attendanceData8.filter(function(item){
                           return (item.students == student_id);
                           });
                        }  
                    break;
                  }
                  case 8:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData9= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:9}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData9 = attendanceData9.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData9.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }));
                          console.log(attendanceData9);
                        }
                        else
                        {
                           var attendanceData9= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:9}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData9 = attendanceData9.filter(function(item){
                           return (item.students == student_id);
                           });
                         }    
                    break;
                  }
                  case 9:
                  {
                   
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData10= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:10}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData10 = attendanceData10.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData10.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData10);
                        }
                        else
                        {
                           var attendanceData10= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:10}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData10 = attendanceData10.filter(function(item){
                           return (item.students == student_id);
                           });
                         }    
                    break;
                  }
                  case 10:
                  {
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData11= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:11}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData11 = attendanceData11.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData11.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData11);
                        }
                        else
                        {
                           var attendanceData11= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:11}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData11 = attendanceData11.filter(function(item){
                           return (item.students == student_id);
                           });
                         }        
                    break;
                  }
                  case 11:
                  {
                    
                          //Code for filtring data month wise
                        if(j==closeMonth-1)
                        {  
                          var attendanceData12= await Attendance.aggregate([
                            {
                              $project:
                              {
                                status:1,
                                attendance_date:1,
                                month:{$month:"$attendance_date"},
                                students:1,
                                _id:0
                              }},
                              {$match:{month:12}}
                         ]);
                         //code for filtring data student_id wise
                         attendanceData12 = attendanceData12.filter(function(item){
                          return (item.students == student_id);
                          });
                          attendanceData12.push(
                            Object.assign(professional, {
                              "students":student_id,
                              "Date":date,
                              "month":month
                            }))
                          console.log(attendanceData12);
                        }
                        else
                        {
                           var attendanceData12= await Attendance.aggregate([
                             {
                               $project:
                               {
                                 status:1,
                                 attendance_date:1,
                                 month:{$month:"$attendance_date"},
                                 students:1,
                                 _id:0
                               }},
                               {$match:{month:12}}
                          ]);
                         //code for filtring data student_id wise
                           attendanceData12 = attendanceData12.filter(function(item){
                           return (item.students == student_id);
                           });
                         }        
                    break;
                  }
                  default :
                  {

                  }
                
                }
              } 
            }
            else
            {
              console.log("ravi");
            }

            //JSON Data  
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({Name:nameText,Class:className,Section:classSection,Jan:attendanceData1,Feb:attendanceData2,March:attendanceData3,April:attendanceData4,May:attendanceData5,June:attendanceData6,July:attendanceData7,Aug:attendanceData8,Sep:attendanceData9,Oct:attendanceData10,Nov:attendanceData11,Dec:attendanceData12},null,3));
          }
        //}
              
    }
    else 
    {

           res.status(404).send("False");
    }


});
module.exports = route;