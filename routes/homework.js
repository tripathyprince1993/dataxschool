const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });
//const Attendance = mongoose.model("Attendance");
const Classrooms = mongoose.model("Classrooms");
const Homeworks = mongoose.model("Homeworks");


router.get("/", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/create_homework.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});
router.get("/Edit", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/Edit_homework.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});
router.get("/View", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                const result = await Classrooms.find({},{ class_id: 1, class_Name: 1, class_section: 1 } )
                res.render(__basedir + '/views/View_homework.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                        classsec : result
                });
        } else {

                res.redirect("/Login");
        }
});




// APIs will begin after here
router.post("/API/Create_Homework", async (req, res) => {
    
        console.log(req.body);
        var body = JSON.stringify(req.body);
      //  console.log(body.length)
            var homeworkdate = req.body.homework_date;
            var className = req.body.Class_id;
            var sectionid = req.body.section_id;
            var homeworktype = req.body.homeworkType;
            var comment = req.body.comment ;
            var  allSection =  req.body.AllSection;
         
        var query = [];
            if(homeworktype== "Class"){
                console.log("classwise data insert ")
                for (i=0;i< allSection.length;i++) {
                query.push({homework_date: homeworkdate ,ClassName:className,SectionID:allSection[i],homework_type:homeworktype,comments:comment/* created_by,creation_date,updated_by:updation_date: */});
        
        }
        }else {
                console.log("sectionwise data insert ")
                query.push({homework_date: homeworkdate ,ClassName:className,SectionID:sectionid,homework_type:homeworktype,comments:comment/* created_by,creation_date,updated_by:updation_date: */});
        
     
            }
            console.log(query);
            // save query 

             // documents array
       
                // save multiple documents to the collection referenced by Book Model
                Homeworks.insertMany(query, function(error, docs) {
                if (error){ 
                return console.error(error);
                } else {
                console.log("Multiple documents inserted to Collection");
                var formData = {
                        Result: "Successfully saved attendence"
                     }
             
           res.send(JSON.stringify(formData));
                }
                });
      
   

});



// APIs will begin after here
router.post("/API/ViewAttendence", async (req, res) => {

        console.log(req.body.attendence_date,req.body.Class_id)
      
        var query = { attendance_date : req.body.attendence_date , Class_id : req.body.Class_id };
        const attendanceData =await Attendance.find(query).populate({ path: 'students', select: ['first_name','middle_name','last_name','roll_No']});

           console.log(attendanceData)

     
   res.send(attendanceData);

});


router.post("/API/CheckAttendence", async (req, res) => {

        console.log(req.body.attendence_date,req.body.Class_id)
      
        var query = { attendance_date : req.body.attendence_date , Class_id : req.body.Class_id };
        const attendancecount =await Attendance.find(query).count();

        var data_return = {
                attendancecount: attendancecount
         }
         res.send(data_return);
           //console.log(attendancecount)

});




router.post("/API/GetbyStudent", async (req, res) => {

       // console.log(req.body.attendence_date,req.body.Class_id)
      
       // var query = { attendance_date : req.body.attendence_date , Class_id : req.body.Class_id };
//        const attendancecount =await Attendance.find(query).count();
      //  const attendanceData =await Calenders.find().populate({ path: 'attendances'});
        //console.log(attendanceData)

        await Calenders.aggregate([
             { $lookup:
                  {
                    from: 'attendances',
                    localField: 'calender_date',
                    foreignField: 'attendance_date',
                    as: 'attendence'
                  }
                }
              ])



        var data_return = {
                attendancecount: attendanceData
         }
         res.send(data_return);
           //console.log(attendancecount)


});

module.exports = router;