const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Calenders=mongoose.model("Calenders");

router.get("/", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
               // const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/AnnualCalander.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: ""
                });




        } else {

                res.redirect("/Login");

        }

});



router.get("/Edit", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
               // const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/Edit_AnnualCalander.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: ""
                });




        } else {

                res.redirect("/Login");

        }

});

router.get("/View", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                var xx = new Date("2019/05/02");
                console.log(xx)
                const result = await Calenders.find({},{calender_date:1 ,calender_day:1,select_type:1,comments:1})
                // res.send(classroom);
                res.render(__basedir + '/views/View_AnnualCalander.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });

        } else {

                res.redirect("/Login");

        }

});


router.post("/",async(req,res)=>{
  
        const calenders=new Calenders();
       calenders.calender_date=req.body.calender_date;
       calenders.calender_day=req.body.calender_day;
       calenders.select_type=req.body.select_type;
       calenders.comments=req.body.comments;
       calenders.created_by=req.body.created_by;
       calenders.creation_date=req.body.creation_date;
       calenders.updated_by=req.body.updated_by;
       calenders.updation_by=req.body.updation_by;

        await calenders.save();
        res.send(calenders);
});




// apis will be followed by here 
router.post("/API/SubmitCalander",async(req,res)=>{
  
        const calenders=new Calenders();
        console.log(req.body)
        
       calenders.calender_date=req.body.calender_date;
       calenders.calender_day=req.body.calender_day;
       calenders.select_type=req.body.select_type;
       calenders.comments=req.body.comments;
       calenders.created_by=req.body.created_by;
       //calenders.creation_date=req.body.creation_date;
      // calenders.updated_by=req.body.updated_by;
     //  calenders.updation_by=req.body.updation_by;

        await calenders.save();
        res.send(calenders);
});





module.exports = router;