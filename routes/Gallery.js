const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});

router.get("/", async (req, res) => {

    sess = req.session;
    //Session set when user Request our app via URL
  //  console.log(sess);
    if (sess.MobNumber) {
           /*
           * This line check Session existence.
           * If it existed will do some action.
           */
           res.render(__basedir + '/views/Gallery.ejs', {
                  Name: sess.Name,
                  mobno: sess.MobNumber,
                  userid:sess.id
           });
    }
    else {
           res.redirect("/Login");
    }

    //const admin=await Admins.find({})
    //res.send(admin);

});
module.exports=router;