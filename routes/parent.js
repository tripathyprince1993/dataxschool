const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});

const Parents=mongoose.model("Parents");

router.get("/",async(req,res)=>{
  
       const parent=await Parents.find({})
       res.send(parent);
   
});
router.post("/",async(req,res)=>{
  
       const parents=new Parents();
       parents.first_name=req.body.first_name;
       parents.last_name=req.body.last_name;
       parents.email=req.body.email;
       parents.password=req.body.password;
       parents.students=req.body.students;
       parents.phone_number=req.body.phone_number;
       parents.address=req.body.address;
       parents.appid = req.body.appid;
       parents.profession=req.body.profession;
       parents.created_by=req.body.created_by;
       parents.creation_date=req.body.creation_date;
       parents.updated_by=req.body.updated_by;
       parents.updation_date=req.body.updation_date;
        await parents.save();
        res.send(parents);
})
module.exports=router;