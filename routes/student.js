const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});

const Students=mongoose.model("Students");

router.get("/",async(req,res)=>{
    
       const student=await Students.find({});
       res.send(student);
   
});




/* router.post("/",async(req,res)=>{
  
        const students=new Students();
        students.student_id=req.body.student_id;
        students.admission_number=req.body.admission_number;
        students.first_name=req.body.first_name;
        students.middle_name=req.body.middle_name;
        students.last_name=req.body.last_name;
        students.birth_date=req.body.birth_date;
        students.sex=req.body.sex;
        students.religion=req.body.religion;
        students.blood_group=req.body.blood_group;
        students.address=req.body.address;
        students.father_contact_number=req.body.father_contact_number;
        students.mother_contact_number=req.body.mother_contact_number;
        students.email=req.body.eamil;
        students.password=req.body.password;
        students.father_name=req.body.father_name;
        students.mother_name=req.body.mother_name;
        students.classrooms=req.body.classrooms;
        students.parent=req.body.parent;
        students.roll_no=req.body.roll_no;
        students.created_by=req.body.created_by;
        students.creation_date=req.body.creation_date;
        students.updated_by=req.body.updated_by;
        students.updation_date=req.body.updation_date;

        await students.save();
        res.send(students);
}); */




// API Codes are from Here  
//  
router.post("/API/GetStudentDetails",async(req,res)=>{
  
       // res.send(students);
       //const student=await Students.find({});
       //res.send(student);
       var classid = mongoose.Types.ObjectId(req.body.class_id);
       
       //console.log("Post Request got from login ", MobNumber, passwd);
       var query = { class_id : classid };
       console.log(classid);

      await  Students.find(query, (err, student_data) => {
             // console.log(student_data);
              if (!err) {
                     console.log(student_data)
                     res.send(student_data);
              } else {
                     console.log("Found post request error in login user ", err);
                     res.send(err);
              }
       });


});

router.post("/API/GetStudentbySection",async(req,res)=>{
  
       // res.send(students);
       //const student=await Students.find({});
       //res.send(student);
       var classid =  mongoose.Types.ObjectId(req.body.class_id)
       //console.log("Post Request got from login ", MobNumber, passwd);
       var query = { class_id : classid };
       console.log(query)
       
       Students.find(query,{ _id: 1,first_name: 1,middle_name: 1,last_name:1 }, (err, student_data) => {
             // console.log(student_data);
              if (!err) {
                     res.send(student_data);
              } else {
                     console.log("Found post request error in login user ", err);
                     res.send(err);
              }
       });


});




module.exports=router;