const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });


const Students=mongoose.model("Students");
const Classrooms = mongoose.model("Classrooms");
const Announcement = mongoose.model("Announcements");


router.get("/", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/Announcement.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});

router.get("/Edit", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/Edit_Announcement.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});

router.get("/View", async (req, res) => {

        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                //console.log("admin"+sess.MobNumber+"  "+sess.Name);
                //res.sendFile(__dirname + "/views/index.html");
                const result = await Classrooms.find({})
                // res.send(classroom);
                res.render(__basedir + '/views/View_Announcement.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.id,
                        classsec: result
                });




        } else {

                res.redirect("/Login");

        }

});



router.post("/API/CreateAnnouncement", async (req, res) => {

       console.log(req.body);
       var AnnouncementType = req.body.NotificationType;
       var Announcementtext = req.body.Announcement;
       var CreatedBy =   req.body.CreatedBy;
        var  className = req.body.class; 
        var SectionVal =  req.body.section;
        var   student_id = req.body.student
       var query = [];
       var currentDate =  Date();
console.log(CreatedBy)
      const student=  await Students.aggregate([ { $lookup: {from: "Classrooms" , localField: "class_id",  foreignField: "_id", as: 'classrooms' }}]);
    //  const student = await Students.find({}) ; 
    

    const Classroom = await  Classrooms.aggregate([{ $lookup: {from: "Students" , localField: "_id",  foreignField: "class_id", as: 'student' }}]);
      //   console.log(student,Classroom)
       
        
       if (AnnouncementType == "All Student"){
      //  console.log(student.length)
        // GetAllStudent with their class id & nae 
        for(var i = 0; i < student.length; i++)
                {
              //   console.log(student[i].class_id , "5cbfe04357c0a32f30710ea0") 
              for(var j=0;j< Classroom.length;j++){
                
                if(student[i].class_id.equals(Classroom[j]._id))
                {
        //                console.log(student[i],student[i].class_id,Classroom[j]._id,Classroom[j].class_Name)
                        //push to json 
                        //console.log(student[i]._id)
                      //   query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:mongoose.Types.ObjectId(student[i].class_id),student_id :mongoose.Types.ObjectId(student[i]._id),annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active",created_by:mongoose.Types.ObjectId(CreatedBy),creation_date:currentDate,updated_by:mongoose.Types.ObjectId(CreatedBy),updation_date:currentDate })
                      query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:student[i].class_id,student_id :student[i]._id,annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active"/*, created_by: CreatedBy,creation_date:currentDate,updated_by:CreatedBy,updation_date:currentDate  */})
                }
              }
             }


       } else if (AnnouncementType == "Student Class Wise"){
        for(var i = 0; i < student.length; i++) {
              //   console.log(student[i].class_id , "5cbfe04357c0a32f30710ea0") 
              for(var j=0;j< Classroom.length;j++){
               
                if(student[i].class_id.equals(Classroom[j]._id) && Classroom[j].class_Name == className )
                {
                        console.log("lass wise")
                        //push to json 
                        //console.log(student[i]._id)
                      //   query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:         ].class_id),student_id :mongoose.Types.ObjectId(student[i]._id),annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active",created_by:mongoose.Types.ObjectId(CreatedBy),creation_date:currentDate,updated_by:mongoose.Types.ObjectId(CreatedBy),updation_date:currentDate })
                      query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:student[i].class_id,student_id :student[i]._id,annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active"/* ,created_by: CreatedBy,creation_date:currentDate,updated_by:CreatedBy,updation_date:currentDate */ })
                }
              }
             }


       } else if (AnnouncementType == "Student Section Wise"){
        console.log("sec wise")
                
        for(var i = 0; i < student.length; i++) {
                //   console.log(student[i].class_id , "5cbfe04357c0a32f30710ea0") 
                for(var j=0;j< Classroom.length;j++){
                  if(student[i].class_id.equals(Classroom[j]._id) && SectionVal == Classroom[j]._id )
                  {
                        query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:student[i].class_id,student_id :student[i]._id,annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active"/* ,created_by: CreatedBy,creation_date:currentDate,updated_by:CreatedBy,updation_date:currentDate  */})
                  }
                }
               }
        
       } else if (AnnouncementType == "Individual Student"){
                
        for(var i = 0; i < student.length; i++) {
                if(student_id == student[i]._id  ) {
                        for(var j=0;j< Classroom.length;j++){
                                if (Classroom[j]._id == SectionVal){
                                        query.push( { announcement_type: AnnouncementType , class_Name : Classroom[j].class_Name,section_id:student[i].class_id,student_id :student[i]._id,annoucement_content :Announcementtext,annoucement_date :currentDate,status:"Active"/* ,created_by: CreatedBy,creation_date:currentDate,updated_by:CreatedBy,updation_date:currentDate  */})
               
                                }
                                
                        }
                        
            }
                }
               

        
        }

        console.log(query);


        var formData = {
                Result: "Successfully saved Announcement"
             }
     
      //  res.send(JSON.stringify(formData))
         Announcement.insertMany(query, function(error, docs) {
                if (error){ 
                return console.error(error);
                } else {
                console.log("Multiple documents inserted to Collection");
                var formData = {
                        Result: "Successfully saved attendence"
                     }
             
           res.send(JSON.stringify(formData));
                }
                }); 
      


      
})
module.exports = router;