const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();
var session = require('express-session');

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Admins = mongoose.model("Admins");

router.get("/", async (req, res) => {

       //const admin = await Admins.find({})

       sess = req.session;
       //Session set when user Request our app via URL
       if (sess.MobNumber) {
              /*
              * This line check Session existence.
              * If it existed will do some action.
              */
              res.render(__basedir + '/views/index.ejs', {
                     Name: sess.Name,
                     mobno: sess.MobNumber,
                     userid:sess.id
              });
       }
       else {
              res.render(__basedir + '/views/login.html', {
                     admminData: ""
              });
       }

});


router.get("/Login", async (req, res) => {

       sess = req.session;
       if (sess.MobNumber) {
              /*
              * This line check Session existence.
              * If it existed will do some action.
              */
              res.redirect("/admin");
       }
       else {
              //res.render(__basedir +'/views/login.html', { 
              //     admminData : admin
              //   });
              res.sendfile(__basedir + "/views/login.html");
       }

});

router.get("/Logout", async (req, res) => {
       req.session.destroy(function (err) {
              if (err) {
                     console.log(err);
              } else {
                     res.redirect('/');
              }
       });
});



// Login Post  Method to authenticate user 
router.post("/LoginUser", async (req, res) => {

       var MobNumber = req.body.MobileNo;
       var passwd = req.body.passwdd;
       //console.log("Post Request got from login ", MobNumber, passwd);
       var query = { contact_number: MobNumber, password: passwd };
       Admins.findOne(query, (err, docs) => {
             // console.log(docs);
              if (!err && docs != null) {
                     // console.log("Found Data",docs.contact_number);
                     //res.send(docs);
                     sess = req.session;
                     sess.MobNumber = docs.contact_number;
                     sess.Email = docs.email;
                     sess.Name = docs.first_name + " " + docs.last_name;
                     sess.Userid = docs._id;
                     
                     var data_return = {
                            text: "Found User "
                     }
                     res.send(data_return);
              } else {

                     console.log("Found post request error in login user ", err);
                     res.send(err);
              }


       });


});

router.post("/RegisterLoginUser", async (req, res) => {


       const admins = new Admins();
       admins.admin_id = req.body.admin_id;
       admins.first_name = req.body.first_name;
       admins.middle_name = req.body.middle_name;
       admins.last_name = req.body.last_name;
       admins.email = req.body.email;
       admins.password = req.body.password;
       admins.contact_number = req.body.contact_number;
       admins.created_by = req.body.created_by;
       admins.creation_date = req.body.creation_date;
       admins.updated_by = req.body.updated_by;
       admins.updation_date = req.body.updation_date;

       await admins.save();
       res.send(admins);
});
module.exports = router;