const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});

const Classrooms=mongoose.model("Classrooms");



router.get("/Create", async (req, res) => {
        //const class_attendance=await Attendance.find({})
        //res.send(class_attendance);
        sess = req.session;
        if (sess.MobNumber) {
                res.render(__basedir + '/views/CreateClass.ejs', {
                        Name: sess.Name,
                        mobno: sess.MobNumber,
                        userid:sess.Userid,
                       
                });
        } else {

                res.redirect("/Login");
        }
});


router.post("/",async(req,res)=>{
  
        const classrooms=new Classrooms();
        classrooms.class_id=req.body.class_id;
        classrooms.class_name=req.body.class_name;
        classrooms.class_section=req.body.class_section;
        classrooms.status=req.body.status;
        classrooms.remarks=req.body.remarks;
        classrooms.teachers=req.body.teachers;
        classrooms.students=req.body.students;
        classrooms.created_by=req.body.created_by;
        classrooms.creation_date=req.body.creation_date;
        classrooms.updated_by=req.body.updated_by;
        classrooms.updation_date=req.body.updation_date;

        await classrooms.save();
        res.send(classrooms);
});



// APIs will begin after here
router.post("/API/Create_Classroom", async (req, res) => {
    
        console.log(req.body)
         var ClassName = req.body.className;
         var class_section = req.body.section;
         var status = req.body.status;
         var created_by  = mongoose.Types.ObjectId(req.body.loggedinUser);
        var remarks = req.body.remarks;
        var query = { class_Name : ClassName , class_section: class_section ,status:status,remarks:remarks,created_by:created_by,updated_by:created_by};
      console.log(query)
      Classrooms.insertMany(query, function(error, docs) {
        if (error){ 
        return console.error(error);
        } else {
        console.log("Multiple documents inserted to Collection");
        var formData = {
                Result: "Successfully saved Classroom"
             }
     
   res.send(JSON.stringify(formData));
        }
        }); 
      
      
      
      
 

});
module.exports=router;