const router=require("express").Router();
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
require('dotenv').config();

mongoose.connect(process.env.MONGOURI,{useNewUrlParser:true});
const FeeCategory=mongoose.model('FeeCategory');
const Classrooms = mongoose.model("Classrooms");
const FeeSubCategory=mongoose.model('FeeSubCategory');
const FeeType=mongoose.model('FeeType');
const FeeTypeDetails=mongoose.model('FeeTypeDetails');
const Students=mongoose.model("Students");
const PaymentDetails = mongoose.model("PaymentDetails");

router.get("/Category", async (req, res) => {

    sess = req.session;
    //Session set when user Request our app via URL
  //  console.log(sess);
    if (sess.MobNumber) {
           /*
           * This line check Session existence.
           * If it existed will do some action.
           */
           res.render(__basedir + '/views/FeeCategory.ejs', {
                  Name: sess.Name,
                  mobno: sess.MobNumber,
                  userid:sess.id
           });
    }
    else {
           res.redirect("/Login");
    }

    //const admin=await Admins.find({})
    //res.send(admin);

});


router.get("/SubCategory", async (req, res) => {

    sess = req.session;
    //Session set when user Request our app via URL
  //  console.log(sess);
    if (sess.MobNumber) {
           /*
           * This line check Session existence.
           * If it existed will do some action.
           */
          const Feeresult = await FeeCategory.find({},{ Fee_Category_Type: 1} )
          const  Feetyperesult = await FeeType.find({},{ Fee_Type: 1} )

           res.render(__basedir + '/views/FeeSubCategory.ejs', {
                  Name: sess.Name,
                  mobno: sess.MobNumber,
                  userid:sess.id,
                  FeeCategory:Feeresult,
                  FeeType:Feetyperesult

           });
    }
    else {
           res.redirect("/Login");
    }

    //const admin=await Admins.find({})
    //res.send(admin);

});



router.get("/Payment", async (req, res) => {

       sess = req.session;
       //Session set when user Request our app via URL
     //  console.log(sess);
       if (sess.MobNumber) {
              /*
              * This line check Session existence.
              * If it existed will do some action.
              */
             const Feeresult = await FeeCategory.find({},{ Fee_Category_Type: 1,Receipt_No_Prefix:1} )
             const  Feetyperesult = await FeeType.find({},{ Fee_Type: 1} )

             const Classresult = await Classrooms.find({},{})
   
              res.render(__basedir + '/views/FeePayment.ejs', {
                     Name: sess.Name,
                     mobno: sess.MobNumber,
                     userid:sess.id,
                     FeeCategory:Feeresult,
                     FeeType:Feetyperesult,
                     ClassRoom : Classresult
   
              });
       }
       else {
              res.redirect("/Login");
       }
   
       //const admin=await Admins.find({})
       //res.send(admin);
   
   });
   


   router.post("/API/GetSubCategoryByCategory",async(req,res)=>{
       console.log(req.body)
      
     var Category = req.body.Fee_Category;

       var query = {Fee_Category : Category};
      console.log(query)
       
              FeeSubCategory.find( query,{Fee_Sub_Category_Name:1,Amount:1} , (err, result) => {
                     // console.log(student_data);
                      if (!err) {
                             res.send(result);
                      } else {
                             console.log("Found post request error  ", err);
                             res.send(err);
                      }
               });


     
   });
  
   router.post("/API/GetFeeTypeDetialsByCategory",async(req,res)=>{
       console.log(req.body)
      
     var SubCategory = req.body.Fee_SubCategory;

       var query = {Fee_SubCategory : SubCategory};
      console.log(query)
       
       FeeTypeDetails.find( query,{FeeTypeText:1} , (err, result) => {
                     // console.log(student_data);
                      if (!err) {
                             res.send(result);
                      } else {
                             console.log("Found post request error  ", err);
                             res.send(err);
                      }
               });


     
   });
    
     
   router.post("/API/GetStudentFeeDetails",async(req,res)=>{
       console.log(req.body)
      
     var SubCategory = req.body.Fee_SubCategory;
       var studentid = req.body.studentid
       var query = {_id : SubCategory};
       
         const Amount =await FeeSubCategory.findOne(query,{Amount:1});
         
       var query = {_id : studentid};
         const Stundetdetials = await Students.findOne(query,{admission_number:1});
       //res.send(admin);
       console.log(Stundetdetials,Amount)
       if (Amount != "" && Stundetdetials != ""){
              var formData = {
                     Student : Stundetdetials,
                     Amount : Amount
                  }
              console.log(formData)
              res.send(formData)
       }

     
   });
    

router.post("/SaveCategory",async(req,res)=>{
    console.log(req.body)
    console.log(req.body.Category)
    var Category = req.body.Category;
    var Prefix = req.body.Prefix;
    var Description = req.body.Description;

    var query = { Fee_Category_Type : Category , Receipt_No_Prefix : Prefix ,Description:Description};
    FeeCategory.insertMany(query, function(error, docs) {
        if (error){ 
        return console.error(error);
        } else {
        //console.log("Multiple documents inserted to Collection");
        var formData = {
                Result: "Fee category Successfully created "
             }
     
            res.redirect("/Fee/Category");
        }
        });
 
  //console.log(Category+" - "+Prefix+"--"+Description)
  
  
  


});


router.post("/SaveSubCategory",async(req,res)=>{
       console.log(req.body)
       var FeeCategory = mongoose.Types.ObjectId(req.body.FeeCategory);
       var SubCategory = req.body.SubCategory;
       var Amount = req.body.Amount;
       var FeeType = req.body.FeeType;


       var query = { Fee_Category : FeeCategory , Fee_Sub_Category_Name : SubCategory ,Amount:Amount,Fee_Type:FeeType};
      
       FeeSubCategory.insertMany(query, function(error, docs) {
           if (error){ 
           return console.error(error);
           } else {
           //console.log("Multiple documents inserted to Collection");
           var formData = {
                   Result: "Fee SubCategory Successfully created "
                }
        
               res.send(formData);
           }
           });
   
   });
   



router.post("/SaveType",async(req,res)=>{

    var Type = "";

    var query = { Fee_Type : Type };
    FeeType.insertMany(query, function(error, docs) {
        if (error){ 
        return console.error(error);
        } else {
        //console.log("Multiple documents inserted to Collection");
        var formData = {
                Result: "Fee category Successfully created "
             }
     
            res.send(docs);
        }
        });
 
  

});



router.post("/SaveTypeDetails",async(req,res)=>{

     
       var Fee_Type =mongoose.Types.ObjectId("5d05408b85449415d0fb8ab7");
       var SubCategory= mongoose.Types.ObjectId("5d0f4fdbb4f4523498734f34");
       var Feetext="Annual()"  ;
       var Month_Start_Date  = new Date().toISOString();
       var Month_End_Date = new Date().toISOString() ;
       var  Month_Due_Date = new Date().toISOString()
   
       var query = { Fee_Type : Fee_Type, Fee_SubCategory: SubCategory ,FeeTypeText:Feetext,Month_Start_Date:Month_Start_Date,Month_End_Date:Month_End_Date,Month_Due_Date:Month_Due_Date };
       FeeTypeDetails.insertMany(query, function(error, docs) {
           if (error){ 
           return console.error(error);
           } else {
           //console.log("Multiple documents inserted to Collection");
           var formData = {
                   Result: "Fee category Successfully created "
                }
        
               res.send(docs);
           }
           });
    
     
   
   });


 
router.post("/StudentPayment",async(req,res)=>{

   //    console.log(req.body)
     
      var RecieptName = req.body.Reciept;
   
      const count =await  PaymentDetails.countDocuments();
      var Inc = count+1
      var Paymentid = RecieptName+"-000-"+Inc;
      console.log(Paymentid)
       var ClassName = req.body.ClassName;
       var Section = mongoose.Types.ObjectId(req.body.SectionId);
       var Student_Name = req.body.StudentName;
       var FeeSubType =  mongoose.Types.ObjectId(req.body.FeeSubType);
       var  FeeCategory =  mongoose.Types.ObjectId(req.body.FeeCategory);
       var FeeSubCategory =  mongoose.Types.ObjectId(req.body.FeeSubCategory);
       var AdmissionNumber = req.body.AdmissionNo;
       var StudentID =  mongoose.Types.ObjectId(req.body.StudentId);
       var Fine = req.body.Fine;
       var Amount = req.body.Amount;
       var Status = req.body.Status;
       var PaymentDate = req.body.PaymentDate;
       console.log(req.body.CreatedBy)
       var CreatedBy =  req.body.CreatedBy;
       var Query = {PaymentID : Paymentid, class_Name: ClassName,Student_Name:Student_Name,Section : Section,Fee_TypeDetails:FeeSubType, Fee_Category:FeeCategory, Fee_SubCategory:FeeSubCategory,Student_AdmissonNo : AdmissionNumber, Student_ID :StudentID,   Amount : Amount, Fine :Fine,status:Status,PaymentDate: PaymentDate,created_by:CreatedBy}
      console.log(Query)
     
       PaymentDetails.insertMany(Query, function(error, docs) {
           if (error){ 
           return console.error(error);
           } else {
           //console.log("Multiple documents inserted to Collection");
           var formData = {
                   Result: "Payment Successfully Submitted "
                }
        
               res.send(docs);
           }
           });
    
     
   
   });

router.get("/PrintOut/:Uid",async(req,res)=>{

       console.log(req.params.Uid);
       // var query = {PaymentID :};
       sess = req.session;
       //Session set when user Request our app via URL
     //  console.log(sess);
       // if (sess.MobNumber) {
              /*
              * This line check Session existence.
              * If it existed will do some action.
              */
              PaymentDetails.findOne({ PaymentID:  req.params.Uid })
             .select('_id PaymentID Student_Name Section Fee_TypeDetails Fee_Category Student_ID Amount Fine PaymentDate status')
        
             .populate({path:'Section',select:'class_section class_Name'})
             .populate({path:'Student_ID',select : 'father_Contact_No mother_contact_No sex address'})
             .populate({ path: 'Fee_Category', select: 'Fee_Category_Type Receipt_No_Prefix' })
             .populate({path:'Fee_TypeDetails',select : 'FeeTypeText'}).exec(function(error, Result) {
              /* `bands.members` is now an array of instances of `Person` */
              if (!error){
                     res.render(__basedir + '/views/FeePrintout.ejs', {
                            PaymentResult: Result
          
                     });
              }else {
                     res.redirect("/Login");
              }
            });
             //res.send(Result)
   
              
       // }
       // else {
       //        res.redirect("/Login");
       // }
   
 


});

module.exports=router;