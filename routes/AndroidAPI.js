const router = require("express").Router();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv').config();
var session = require('express-session');
var crypto=require("crypto");
var generator=require('generate-password');

mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true });

const Parent = mongoose.model("Parents");
const Students=mongoose.model("Students");
var textlocal = require("./smsAPI");
//var encrypt_decrypt=require("./encrypt_decrypt");

router.post("/Parent/SendPushNotifiations", async (req, res) => {

       var phoneNumber = req.body.mobileNumber;
       var appid = req.headers.app_id;
    //  console.log(phoneNumber,appid);
     //  console.log(req.body)
      
       var authorizationKey = req.headers.authorization;
       var serverAuthKey = process.env.AuthrizationKey;
       var serverApp_Id =  process.env.UniqApp_id;

       //Encryption_Decryption 
       const key = crypto.randomBytes(32);
       const iv = crypto.randomBytes(16);
       
       function encrypt(text) {
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
       }
       
       function decrypt(text) {
        let iv = Buffer.from(text.iv, 'hex');
        let encryptedText = Buffer.from(text.encryptedData, 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
       }
       
       var enrypt_data = encrypt(appid);
       //console.log(enrypt_data);
       var decrypt_data=decrypt(enrypt_data);
      // console.log(decrypt_data);
       //Random Password generation

       var random_password=generator.generate({
              length:10,
              numbers:true

       });
       // console.log(random_password);

       if (authorizationKey == serverAuthKey && decrypt_data == serverApp_Id ){
         //    console.log("hii");
              var F_query = { father_Contact_No: phoneNumber };
              const student_father =await Students.findOne(F_query,{});
              console.log(student_father);
              var M_query = { mother_contact_No: phoneNumber };
              const student_mother =await Students.findOne(M_query,{});
              //console.log("Mother Data",student_mother);
              
             if (student_father != null )
             { 
               var  studentfather = student_father.toObject();
               //console.log(stundentfather);
               var query = { father_number: phoneNumber };
               var parentData =await Parent.findOne(query,{});
               //console.log(parentData);
                
                if (parentData != null)
                {
                     if( parentData._id.toString()==studentfather.parent_id.toString())
                     {

                            console.log("helloo null password");
                            if (parentData.father_password == null){
                            
                                   // create new password 
                                   const parent= new Parent()
                                   parent.father_password=random_password;

                                   await parent.save();
                                   var Text ="Dear Parent, Your Login Id : "+phoneNumber+" password :"+random_password+" dataxtech.com";
                                   //    var returnval =  textlocal.sendSmSNotifications(phoneNumber,password);
                                          var returnval = "new password created "+random_password;
                                          res.status(200).send("values returned "+returnval+" ----   "+Text);
                            } else {             
                            var passwd =  parentData.father_password ;
                            var enrypt_password = encrypt(passwd);
                            var decrypt_password=decrypt(enrypt_password);
                            var Text ="Dear Parent,  Your password has been reset. Your Login Id : "+phoneNumber+" password :"+decrypt_password+"dataxtech.com";       
                                   var returnval =  textlocal.sendSmSNotifications(phoneNumber,Text);
                                   // console.log(returnval);
                                          if (returnval == "success"){
                                                 res.status(200).send("True");
                                          }else {
                                                 res.status(200).send("False");
                                          }                                 
                     }       }
                 }
                 else
                 {
                     console.log("hi");
                     
                     var studentfather=student_father.toObject();
                     console.log(studentfather);
                     console.log(studentfather._id);

                     // getting  data in from student table table 
                     const parent= new Parent()
                     parent.father_number=studentfather.father_Contact_No;
                     parent.mother_number=studentfather.mother_contact_No;
                     parent.father_password=random_password;
                     parent.mother_password=random_password;
                     // parent.students_objectId=studentfather._id//Adding students _id into parents table, not able to insert

                     // inserting value to the parent table
                     await parent.save();
                     var F_query = { father_number: phoneNumber };
                     const data =await Parent.findOne(F_query,{});
                     var Data=data.toObject();
                     //var d=Data._id;
                     //console.log(d);
                     //updating parent _id into students table.
                     var qq = {father_Contact_No:phoneNumber}
                     await Students.updateMany(qq, { parent_id: Data._id });
                            
                     var Text ="Dear Parent,  Your password has been reset. Your Login Id : "+phoneNumber+" password :"+random_password+"dataxtech.com";     
                                   //    var returnval =  textlocal.sendSmSNotifications(phoneNumber,password);
                                   var returnval =  textlocal.sendSmSNotifications(phoneNumber,Text);
                                   // res.status(200).send("values returned "+returnval);
                                   console.log(returnval);

                                   if (returnval == "success"){
                                          res.status(200).send("True");
                                   }else {
                                          res.status(200).send("False");
                                   }       

                 }
              }
              else if(student_mother != null )
              {
              //       console.log("hii");
               var  studentmother = student_mother.toObject();
               var query = { mother_number: phoneNumber };
               const parentData =await Parent.findOne(query,{});
                if (parentData != null && parentData._id.toString()==studentmother.parent_id.toString())
                {
                     if (parentData.mother_password == null){

                            // create new password 
                            const parent= new Parent()
                            parent.mother_password=random_password;
                            await parent.save();

                           random_password
                              //    var returnval =  textlocal.sendSmSNotifications(phoneNumber,password);
                              var returnval = "new password created "+random_password;
                              res.status(200).send("values returned "+returnval+" ----   "+Text);
                                  

                     } else {
                            var passwd =  parentData.password ;
                            var enrypt_password = encrypt(passwd);
                            var decrypt_password=decrypt(enrypt_password); 
                           
                         
                            var Text ="Dear Parent,  Your password has been reset. Your Login Id : "+phoneNumber+" password :"+decrypt_password+"dataxtech.com";     
                           
                               var returnval =  textlocal.sendSmSNotifications(phoneNumber,Text);
                               console.log(returnval);
                               
                                   if (returnval == "success"){
                                          res.status(200).send("True");
                                   }else {
                                          res.status(200).send("False");
                                   }                                 
                     }
                 }else{

                     // getting  data  from student table  
                     var studentmother=student_mother.toObject();
                     // getting  data in from student table table 
                     const parent= new Parent()
                     parent.father_number=studentmother.father_Contact_No;
                     parent.mother_number=studentmother.mother_contact_No;
                     parent.father_password=random_password;
                     parent.mother_password=random_password;
                    // parent.students_objectId=studentmother._id//Adding students _id into parents table, not able to insert
                     // inserting value to the parent table
                     await parent.save();
                     var F_query = { mother_number: phoneNumber };
                     const data =await Parent.findOne(F_query,{});
                     var Data=data.toObject();
                     
                     //updating parent _id into students table.
                     var qq = {mother_contact_No:phoneNumber}
                     await Students.updateMany(qq, { parent_id: Data._id });
           
                     var Text ="Dear Parent,  Your password has been reset. Your Login Id : "+phoneNumber+" password :"+random_password+"dataxtech.com";     
                     
                    //    var returnval =  textlocal.sendSmSNotifications(phoneNumber,password);
                         var returnval =  textlocal.sendSmSNotifications(phoneNumber,Text);
                        // res.status(200).send("values returned "+returnval);
                        console.log(returnval);

                         if (returnval == "success"){
                                res.status(200).send("True");
                         }else {
                                res.status(200).send("False");
                         }       

                 }

              } 
              
              else 
              {

              console.log("Invalid Parent")
              res.status(404).send("False");
              }

       }
       else 
       {
              console.log("Unauthrized User")
              res.status(404).send("False");
       }
});

module.exports = router;



