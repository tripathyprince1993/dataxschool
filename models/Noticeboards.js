const mongoose=require('mongoose');

const Noticeboard=mongoose.Schema({
    
    notice_body:{type:String,required:false},
    notice_image:{type:String,required:false},
    notice_Postdate :{type:Date,required:false},
    status:{type:String,required:false},
    created_by:{type:mongoose.Schema.Types.ObjectId, ref:"Admins"},
    creation_date:{type:Date,required:false},
    updated_by:{type:mongoose.Schema.Types.ObjectId, ref:"Admins"},
    updation_date:{type:Date,required:false}
});
module.exports=mongoose.model("Noticeboards",Noticeboard);
