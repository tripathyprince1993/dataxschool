const mongoose=require('mongoose')

const loginSchema=mongoose.Schema({
    tokenId:{type:String,required:true},
    parentId:{type:mongoose.Schema.Types.ObjectId, ref:"Parents"},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,default:Date.now},
});
module.exports=mongoose.model("Login",loginSchema);