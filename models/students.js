const mongoose=require("mongoose");

const students_schema=mongoose.Schema({
    student_id:{type:Number,required:true},
    admission_number:{type:Number,required:true},
    first_name:{type:String,required:true},
    middle_name:{type:String,required:false},
    last_name:{type:String,required:false},
    birth_date:{type:String,required:true},
    sex:{type:String,required:true},
    religion:{type:String,required:false},
    blood_group:{type:String,required:false},
    address:{type:String,required:true},
    father_name:{type:String,required:false},
    mother_name:{type:String,required:false},
    father_contact_number:{type:Number,required:true},
    mother_contact_number:{type:Number,required:true},
    //father_email:{type:String,required:true},
    //mother_email:{type:String,required:true},
    father_password:{type:String,required:true},
    mother_password:{type:String,required:true},
    // father_first_name:{type:String,required:true},
    // father_last_name:{type:String,required:true},
    // mother_first_name:{type:String,required:true},
    // mother_last_name:{type:String,required:true},
    father_profession:{type:String,required:false},
    mother_profession:{type:String,required:false},
    classrooms:{type:mongoose.Schema.Types.ObjectId,ref:"Classrooms"},
    parent_id:{type:mongoose.Schema.Types.ObjectId, ref:"Parents"},
    roll_no:{type:Number,required:true},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,default:Date.now}
},
{
    timestamps:true
}
);
module.exports=mongoose.model("Students",students_schema);
