const mongoose=require('mongoose');

const parents_schema=mongoose.Schema({
   // father_first_name:{type:String,required:true},
   // father_last_name:{type:String,required:false},
   // mother_first_name:{type:String,required:true},
   // mother_last_name:{type:String,required:false},
    //name:{type:String,required:true},
    //mother_name:{type:String,required:true},
   // email:{type:String,required:true},
    father_password:{type:String,required:true},
    mother_password:{type:String,required:true},
    students:{type:mongoose.Schema.Types.ObjectId, ref:'Students',required:false},
    father_number:{type:String,required:false},
    mother_number:{type:String,required:false},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,default:Date.now},
})
module.exports=mongoose.model("Parents",parents_schema)
