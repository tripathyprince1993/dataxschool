const mongoose=require('mongoose');

const calender_schema=mongoose.Schema({
    calender_date:{type:Date,required:"Date required"},
    calender_day:{type:String,required:true},
    select_type:{type:String,required:true},
    comments:{type:String,required:false},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}

})
module.exports=mongoose.model("Calenders",calender_schema);