const mongoose=require("mongoose");

const teachers_schema=mongoose.Schema({
    class_teacher_id:{type:Number,required:true},
    classrooms:{type:mongoose.Schema.Types.ObjectId, ref:"Classrooms"},
    first_name:{type:String,required:true},
    middle_name:{type:String,required:false},
    last_name:{type:String,required:false},
    birth_date:{type:String,required:false},
    sex:{type:String,required:false},
    religion:{type:String,required:false},
    blood_group:{type:String,required:false},
    address:{type:String,required:false},
    phone:{type:String,required:true},
    email:{type:String,required:true},
    password:{type:String,required:true},
    created_by:{type:String,required:false},
    crretion_date:{type:Date,default:Date.now},
    updation_by:{type:String,required:false},
    updated_date:{type:Date,default:Date.now}
})
module.exports=mongoose.model("Teachers",teachers_schema);