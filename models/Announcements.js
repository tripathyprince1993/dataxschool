const mongoose=require('mongoose');

const Announcement_schema =mongoose.Schema({
    announcement_type: {type:String,required:true},
    class_Name :{type:String,required:true},
    section_id :{type:mongoose.Schema.Types.ObjectId, ref:"Classrooms"},
    student_id :{type:mongoose.Schema.Types.ObjectId, ref:"Students"},
    annoucement_content :{type:String,required:true},
    annoucement_date :{type:String,required:true},
    status:{type:String,required:true},
    created_by:{type:mongoose.Schema.Types.ObjectId, ref:"Admins"},
    creation_date:{type:Date,required:false},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false}
});
module.exports=mongoose.model("Announcements",Announcement_schema);
