const mongoose=require('mongoose');

const homework=mongoose.Schema({
    
    homework_date:{type:Date,required:false},
    ClassName:{type:String,required:false},
    SectionID : {type:mongoose.Schema.Types.ObjectId, ref:"Classrooms"},
    comments:{type:String,required:false},
    homework_type:{type:String, required:false},
    created_by:{type:mongoose.Schema.Types.ObjectId, ref:"Admins"},
    creation_date:{type:Date,required:false},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false}
});
module.exports=mongoose.model("Homeworks",homework);
