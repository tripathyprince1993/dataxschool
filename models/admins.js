const mongoose=require('mongoose');

const admin_schema=mongoose.Schema({
    admin_id:{type:Number,required:true},
    first_name:{type:String,required:true},
    middle_name:{type:String,required:false},
    last_name:{type:String,required:true},
    email:{type:String,required:true},
    password:{type:String,required:true},
    contact_number:{type:Number,required:true},
    created_by:{type:String,required:true},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:true},
    updation_date:{type:Date,default:Date.now},
})
module.exports=mongoose.model("Admins",admin_schema);

