const mongoose=require('mongoose');
 
const FeeCategorySchema =mongoose.Schema({
    Fee_Category_Type:{type:String,required:true},
    Receipt_No_Prefix:{type:String,required:true},
    Description:{type:String,required:true},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}

})
module.exports=mongoose.model("FeeCategory",FeeCategorySchema);

// Fee SubCategory 
const FeeSubCategorySchema =mongoose.Schema({
    Fee_Category:{type:mongoose.Schema.Types.ObjectId,ref:"FeeCategory"},

    Fee_Sub_Category_Name:{type:String,required:true},
    Amount:{type:Number,required:true},
    Fee_Type:{type:mongoose.Schema.Types.ObjectId,ref:"FeeType"},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}

});
module.exports=mongoose.model("FeeSubCategory",FeeSubCategorySchema);
 
const FeeTypeSchema =mongoose.Schema({
    Fee_Type:{type:String,required:true},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}

});
module.exports=mongoose.model("FeeType",FeeTypeSchema);

const FeeTypeDetailsSchema =mongoose.Schema({
    Fee_Type:{type:mongoose.Schema.Types.ObjectId,ref:"FeeType"},
    Fee_SubCategory:{type:mongoose.Schema.Types.ObjectId,ref:"FeeSubCategory"},
    FeeTypeText : {type:String,required:false},
    Month_Start_Date:{type:Date,required:false},
    Month_End_Date:{type:Date,required:false},
    Month_Due_Date:{type:Date,required:false},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}
});
module.exports=mongoose.model("FeeTypeDetails",FeeTypeDetailsSchema);

const PaymentDetailsSchema  =mongoose.Schema({
    PaymentID : {type:String,required:true},
    class_Name : {type:String,required:false},
    Section : {type:mongoose.Schema.Types.ObjectId,ref:"Classrooms"},
    Fee_TypeDetails:{type:mongoose.Schema.Types.ObjectId,ref:"FeeTypeDetails"},
    Fee_Category:{type:mongoose.Schema.Types.ObjectId,ref:"FeeCategory"},
    Fee_SubCategory:{type:mongoose.Schema.Types.ObjectId,ref:"FeeSubCategory"},
    Student_AdmissonNo : {type:String,required:true},
    Student_Name : {type:String,required:true},
    Student_ID :{type:mongoose.Schema.Types.ObjectId,ref:"Students"},
    Amount : {type:Number,required:true},
    Fine :{type:Number,required:true},
    status:{type:String,required:true},
    PaymentDate :{type:Date,required:false},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false,default:Date.now}
});
module.exports=mongoose.model("PaymentDetails",PaymentDetailsSchema);
