const mongoose=require('mongoose');

const classroom_schema=mongoose.Schema({
    class_Name:{type:String,required:true},
    class_section:{type:String,required:true},
    status:{type:String,required:true},
    remarks:{type:String,required:false},    
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,default:Date.now},
})
module.exports=mongoose.model("Classrooms",classroom_schema);