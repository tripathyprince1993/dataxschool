const mongoose=require('mongoose');

const attendance_schema=mongoose.Schema({
    
    students:{type:mongoose.Schema.Types.ObjectId, ref:"Students"},
    attendance_date:{type:Date,required:false},
    status:{type:String,required:false},
    reason:{type:String,required:false},
    remarks:{type:String,required:false},
    Class_id:{type:mongoose.Schema.Types.ObjectId, ref:"Classrooms"},
    created_by:{type:mongoose.Schema.Types.ObjectId, ref:"Admins"},
    creation_date:{type:Date,required:false},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,required:false}
});
module.exports=mongoose.model("Attendance",attendance_schema);
