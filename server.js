const express = require("express");
require('express-async-errors');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var path = require('path');
app.engine('html', require('ejs').renderFile);
var session = require('express-session');

app.set('views', path.join(__dirname, 'views'));

global.__basedir = __dirname;
app.use(express.static(path.join(__dirname, 'vendors')));
app.use(express.static(path.join(__dirname, 'build')));
app.use(express.static(path.join(__dirname, 'public')));


app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: '12345adasdsdsd2e3ewsd3erwdcyhg'
}));

var sess;
// caching disabled for every route
app.use(function(req, res, next) {
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
  });


//Database Connection
require('./mongo_conn');

//Models 
require('./models/admins');
require('./models/students');
require('./models/teachers');
require('./models/attendance');
require('./models/classrooms');
require('./models/parents');
require('./models/calanders');
require('./models/homeworks');
require('./models/Noticeboards');
require('./models/Announcements');
require('./models/calenderyears');
require('./models/Login');
require("./models/FeeModels");


//Middleware
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//Routes
app.use("/", require("./routes/login"));
app.use("/admin", require("./routes/admin"));
app.use("/API", require("./routes/AndroidAPI"));
app.use("/API",require("./routes/AttendanceAPI"));
app.use("/API",require("./routes/LoginAPI"));
app.use("/API",require("./routes/CalenderAPI"));
app.use("/API",require("./routes/ProfileViewAPI"));
app.use("/API",require("./routes/HomeworkAPI"));
app.use("/API",require("./routes/NoticeboardAPI"));

app.use("/Notifications", require("./routes/Notifications"));
app.use("/AnnualCalander", require("./routes/AnnualCalander"));
app.use("/Announcement", require("./routes/Announcement"));
app.use("/student", require("./routes/student"));
app.use("/teacher", require("./routes/teacher"));
app.use("/attendence", require("./routes/class_attendance"));
app.use("/classroom", require("./routes/classroom"));
app.use("/parent", require("./routes/parent"));
app.use("/Location",require("./routes/Location"));
app.use("/Gallery",require("./routes/Gallery"));
app.use("/Video",require("./routes/Video"));
app.use("/Fee",require("./routes/FeeRoutes"));






// Attendence Route  Create / Edit / view for web requests
app.use("/attendence", require("./routes/class_attendance"));

// Homework Route Create/ edit/view for web request
app.use("/homework", require("./routes/homework"));

// Homework Route Create/ edit/view for web request
app.use("/Noticeboard", require("./routes/Noticeboard"));

//Not Found Routs
app.use((req, res, next) => {
    req.status = 404;
    //const error= new Error("Routes not found");
        res.sendFile(__basedir + '/views/page_404.html');

});

//error handler

app.use((error, req, res, next) => {

    //res.sendFile(__dirname + "/views/page_500.html");
    console.log("Error",error);
    res.render(__basedir + '/views/page_500.html', {
        Error: error
    });
});


var port = process.env.port || 1337;
app.listen(port, function () {
    console.log("server is running on port ",port);
})